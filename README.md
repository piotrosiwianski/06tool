# Temat
Narzędzia i obiekty zastępcze.

# Cel
Celem zajęć jest wyjaśnienie techniki obiektów zastępczych (mock objects), wraz z przedstawieniem przykładowych bibliotek oraz wprowadzenie do wybranych elementów, technik i narzędzi pomocniczych takich jak biblioteki asercji czy parametryzacja testów.

# Technologie
* Java
  * Java 8,
  * JUnit 5.x - https://junit.org/junit5/docs/current/user-guide/
  * Mockito - http://mockito.org/
  * AssertJ - http://joel-costigliola.github.io/assertj/
* Python 
  * Python 3.x,
  * pytest - https://docs.pytest.org/en/latest/contents.html
  * unittest - https://docs.python.org/3/library/unittest.html
  * unittest.mock - https://docs.python.org/3/library/unittest.mock.html
* TypeScript
  * Angular 7.x, typescript
  * Karma/Jasmine - https://jasmine.github.io/

## Zadania

### Narzędzia
Należy uzupełnić testy jednostkowe, kładąc nacisk na dobór odpowiednich asercji oraz parametryzację scenariuszy testowych. Można bazować na dowolnym projekcie z testami jednostkowymi, przy czym rekomenduje się, zależnie od technologii:
* Angular/ts - https://gitlab.com/spio-sources/translator/blob/master/atranslator/src/app/gaderypoluki/gaderypoluki-param.spec.ts
* Java/JUnit/AssertJ - https://gitlab.com/spio-sources/translator/-/blob/master/jtranslator/translator/src/test/java/pl/poznan/put/spio/tool/GaDeRyPoLuKiToolTest.java
* python/unittest - https://gitlab.com/spio-sources/translator/blob/master/ptranslator/tests/unittest_gaderypolukiToolTest.py

Każdy projekt oferuje inne biblioteki i cechy językowe, przed przystąpieniem do napisania choćby linijki, należy dokładnie przeczytać kod z komentarzami. Przykładowo, w ramach zadania można uzupełnić dane do translacji w istniejącym scenariuszu, zmienić asercje (np. z JUnit na AssertJ), a także napisać zupełnie nowe scenariusze z wykorzystaniem nowych konstrukcji językowych.

### Obiekty zastępcze
Należy napisać testy jednostkowe dla podanych scenariuszy testowych i podanego fragmentu oprogramowania, wybierając projekt w języku Java lub Python. Wybrane zależności należy zastąpić obiektami stworzonymi z użyciem biblioteki Mockito lub unittest.mock, zależnie od wybranego języka. Należy wykorzystać możliwość rejestrowania interakcji przez obiekt zastępczy.

Przedmiotem testów jest projekt katalogu osobowego, we wczesnej fazie rozwoju. Katalog (klasa Catalog) zakłada możliwość wykonania podstawowych operacji: dodanie osoby, wyszukanie osoby, sprawdzenie numeru pesel, przy czym wszystkie operacje odbywają się na abstrakcyjnym obiekcie bazy danych oraz serwisu (elementy jeszcze niezaimplementowane). Brak bazy danych oraz serwisu powoduje trudność z przetestowaniem obiektu głównego (Catalog).
Scenariusze do przygotowania w ramach zadania:
1. Pobranie pierwszego numeru PESEL dla imienia i nazwiska,
2. Pobranie listy numerów PESEL dla imienia i nazwiska,
3. Wyszukanie osoby po numerze pesel,
4. Dodanie osoby do katalogu, poprawny PESEL.
5. Dodanie osoby do katalogu, niepoprawny PESEL.

Projekt catalog w pythonie: https://gitlab.com/spio-sources/catalog/tree/master/pcatalog

Projekt catalog w Javie: https://gitlab.com/spio-sources/catalog/tree/master/jcatalog
